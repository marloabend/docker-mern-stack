FROM node:erbium-alpine

RUN npm install -g nodemon
RUN mkdir -p /app/backend

WORKDIR /app/backend

COPY package*.json /app/backend/

RUN npm install

COPY . /app/backend/

CMD ["nodemon", "server.js"]
