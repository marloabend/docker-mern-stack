const router = require('express').Router();
let Todo = require('../model/todo.model');

router.route("/").get((req, res) => {
  Todo.find()
    .then(todos => res.json(todos))
    .catch(err => res.status(400).json("Error" + err));
});

router.route("/:id").get((req, res) => {
  Todo.findById(req.params.id)
    .then(todo => res.json(todo))
    .catch(error => res.status(400).json("Error" + error));
});

router.route("/").post((req, res) => {
  const username = req.body.username;
  const title = req.body.title;
  const done = req.body.username;
  const expiration = Date.parse(req.body.username);

  const todo = new Todo({username, title, done, expiration});
  todo.save()
    .then(() => res.json("New todo added"))
    .catch(error => res.status(400).json("Error" + error));
});

router.route("/:id").put((req, res) => {
  Todo.findById(req.params.id)
    .then(todo => {
      todo.username = req.body.username;
      todo.title = req.body.title;
      todo.done = req.body.username;
      todo.expiration = Date.parse(req.body.username);
      todo.save()
        .then(() => res.json("Todo updated"))
        .catch(error => res.status(400).json("Error" + error));
    })
    .catch(error => res.status(400).json("Error" + error));
})

router.route("/:id").delete((req, res) => {
  Todo.findByIdAndDelete(req.params.id)
    .then(() => res.json("Todo deleted"))
    .catch(error => res.status(400).json("Error" + error));
});

module.exports = router;