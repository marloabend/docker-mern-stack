const router = require('express').Router();
let User = require('../model/user.model');

router.route("/").get((req, res) => {
  User.find()
    .then(users => res.json(users))
    .catch(err => res.status(400).json("Error" + err));
});

router.route("/:id").get((req, res) => {
  User.findById(req.params.id)
    .then(user => res.json(user))
    .catch(error => res.status(400).json("Error" + error));
});

router.route("/").post((req, res) => {
  const username = req.body.username;
  const email = req.body.email;

  const user = new User({username, email});
  user.save()
    .then(() => res.json("New user added"))
    .catch(error => res.status(400).json("Error" + error));
});

router.route("/:id").put((req, res) => {
  User.findById(req.params.id)
    .then(user => {
      user.username = req.body.username;
      user.email = req.body.email;
      user.save()
        .then(() => res.json("User updated"))
        .catch(error => res.status(400).json("Error" + error));
    })
    .catch(error => res.status(400).json("Error" + error));
})

router.route("/:id").delete((req, res) => {
  User.findByIdAndDelete(req.params.id)
    .then(() => res.json("User deleted"))
    .catch(error => res.status(400).json("Error" + error));
});

module.exports = router;