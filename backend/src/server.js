const cors = require("cors")
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = process.env.PORT || 8080;
const routePrefix = process.env.NODE_ENV === 'development' ? '/api' : '';

app.use(cors());
app.use(express.json());

const databaseURL = process.env.MONGO_URI;
const options = {
  autoIndex: false,
  poolSize: 10,
  bufferMaxEntries: 0,
  useNewUrlParser: true, // Avoid deprecation warnings
  useUnifiedTopology: true
}

const connect = () => mongoose.connect(databaseURL, options);
connect();
const connection = mongoose.connection;
connection.on('error', error => {
  console.error('An Error occured while connecting to the database: ', error);
  setTimeout(() => connect(), 5000);
});
connection.once('open', () => {
  console.log('MongoDB connected!');
});

// Routing
const userRouter = require('./routes/users');
const todoRouter = require('./routes/todos');
app.use(routePrefix + '/users', userRouter);
app.use(routePrefix + '/todos', todoRouter);
app.listen(port, () => console.log(`Listening on port ${port}`));
