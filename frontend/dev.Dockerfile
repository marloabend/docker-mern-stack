FROM node:erbium-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY package-lock.json ./
RUN npm install
RUN npm install -g react-scripts@3.4.1

COPY . ./

CMD ["npm", "start"]
