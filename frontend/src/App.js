import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './component/navbar.component';
import UsersList from './component/user-list.component';
import CreateUser from './component/create-user.component';
import './App.css';

function App() {
  return (
    <div>
      <Navbar/>
      <div className='App container p-3'>
        <CreateUser/>
        <UsersList/>
      </div>
    </div>
  );
}

export default App;
