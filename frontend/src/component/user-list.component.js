import React, {Component} from 'react';
import axios from 'axios';

export default class UserList extends Component {
  state = {
    users: ['No users']
  };

  componentDidMount = async () => {
    try {
      const res = await axios.get('/api/users');
      console.log('Axios', res.data);
      if (res.data.constructor === Array) {
        this.setState({users: res.data});
      }
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    return (
      <div>
        <h3>User overview</h3>
        <table className="table table-striped table-hover">
          <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th className="text-right">Actions</th>
          </tr>
          </thead>
          <tbody>
          {[1, 2, 3, 4, 5].map(i => (<tr key={i}>
            <td>{i}.</td>
            <td>Entry</td>
            <td className="text-right">
              <button className="btn btn-sm btn-outline-danger">Delete</button>
            </td>
          </tr>))}
          </tbody>
        </table>
      </div>
    );
  }
}
