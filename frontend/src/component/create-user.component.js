import React, {Component} from 'react';
import axios from 'axios';

export default class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeUsername.bind(this);
  }

  state = {
    username: '',
    email: ''
  };

  onChangeUsername(e) {
    this.setState({username: e.target.value})
  }

  onChangeEmail(e) {
    this.setState({email: e.target.value})
  }

  onSubmit(e) {
    e.preventDefault();
    console.log({
      username: this.state.username,
      email: this.state.email
    });

    try {
      const res = axios.post('/api/users', {
        username: this.state.username,
        email: this.state.email
      });
      console.log('Axios', res.data);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <form className="mb-4">
        <h3>Create User</h3>
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input type="text" className="form-control" id="username" value={this.state.username} onChange={this.onChangeUsername} />
        </div>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input type="email" className="form-control" id="email" value={this.state.email} onChange={this.onChangeEmail} />
        </div>
        <input type="submit" className="btn btn-primary" value="Submit" />
      </form>
    );
  }
}
