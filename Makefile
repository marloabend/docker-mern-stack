.PHONY: dev prod build build-prod restart restart-prod down

dev: down
	@docker-compose -f ./docker-compose.dev.yml up

prod: down
	@docker-compose up --build

build:
	@docker-compose -f ./docker-compose.dev.yml build --no-cache

build-prod:
	@docker-compose build --no-cache

restart:
	@docker-compose -f ./docker-compose.dev.yml restart

restart-prod:
	@docker-compose restart

down:
	@docker-compose -f ./docker-compose.dev.yml down --remove-orphans
	@docker-compose down --remove-orphans
